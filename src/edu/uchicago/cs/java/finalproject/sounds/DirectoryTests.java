package edu.uchicago.cs.java.finalproject.sounds;

import java.nio.file.Paths;

/**
 * Created by tcsalameh on 11/23/15.
 */
public class DirectoryTests {
    public static void main(String[] args) {
        System.out.println(System.getProperty("user.dir"));

        System.out.println(Paths.get("keys", "Celesta"));
    }
}
