package edu.uchicago.cs.java.finalproject.sounds;

import java.io.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.sound.sampled.*;

public class Sound {

	//for individual wav sounds (not looped)
	//http://stackoverflow.com/questions/26305/how-can-i-play-sound-in-java
	public static synchronized void playSound(Clip clp) {
		try  {
			//clp.stop();
			clp.setFramePosition(0);
			clp.start();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * creates new clip up to be played, and returns the object with audio loaded. I separated this out because
	 * sometimes opening the stream can hang when many clips are trying to load at once.
	 * It also minimizes latency each time around, which helps with tempo drift.
	 * So for repeating sounds, we load once and play multiple times.
	 * @param strPath
	 * @return Clip object that is open and has an input stream from the resource at strPath
	 */
	public static Clip loadClip(final String strPath) {

		try {
			Clip clp = AudioSystem.getClip();

			//this is OK - usually 1-2 ms, max 15-20 in rare cases
			AudioInputStream aisStream =
					AudioSystem.getAudioInputStream(Sound.class.getResourceAsStream(strPath));

			//this can be a time hog, so we put it in preload procedure
			clp.open(aisStream);
			return clp;
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * This is to close the clip gracefully, once it's done with playing the sound.
	 * We just add a listener to automatically close it out once done with the file.
	 * When I was using the close() method alone, it interfered w/ instantiating other sounds later.
	 * http://stackoverflow.com/questions/1816832/java-clip-sound-audio-memory-leak-after-closing-with-close
	 */
	public static void closeClip(Clip c) {
		c.addLineListener(new LineListener(){
			public void update(LineEvent event){
				if(event.getType() == LineEvent.Type.STOP){
					event.getLine().close();
				}
			}
		});
	}

	//for looping wav clips
	//http://stackoverflow.com/questions/4875080/music-loop-in-java
	public static Clip clipForLoopFactory(String strPath){
		
		Clip clp = null;
		
		// this line caused the original exceptions
		
		try {
			AudioInputStream aisStream = 
					  AudioSystem.getAudioInputStream(Sound.class.getResourceAsStream(strPath));
			clp = AudioSystem.getClip();

			clp.open( aisStream );

				
		} catch (UnsupportedAudioFileException exp) {
			
			exp.printStackTrace();
		} catch (IOException exp) {
			
			exp.printStackTrace();
		} catch (LineUnavailableException exp) {
			
			exp.printStackTrace();
			
		//the next three lines were added to catch all exceptions generated
		}catch(Exception exp){
			System.out.println("error");
		}
		
		return clp;
		
	}
	
	


}
