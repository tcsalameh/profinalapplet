package edu.uchicago.cs.java.finalproject.mvc.controller;

import edu.uchicago.cs.java.finalproject.mvc.model.*;
import edu.uchicago.cs.java.finalproject.mvc.view.GamePanel;

import java.awt.*;
import java.applet.*;
import java.awt.event.*;
import java.util.Random;

// ===============================================
// == This Game class is the CONTROLLER
// ===============================================

public class Game extends Applet implements Runnable, KeyListener, MouseMotionListener, MouseListener {

	// ===============================================
	// FIELDS
	// ===============================================

	public static final Dimension DIM = new Dimension(700, 700); //the dimension of the game - MULTIPLE OF 4.
	public static final TimeSignature TIME_SIGNATURE = new TimeSignature(4,4); // time signature
	private int bpm; // beats per minute
	private NoteIndicator noteIndic; //follows mouse around indicating note
	private TimeGrid timeIndic; //static background anim for what's coming up in the next bar
	private GamePanel gmpPanel;
	public static Random R = new Random();
	public final static int ANI_DELAY = 45; // milliseconds between screen
											// updates (animation)
	private Thread thrAnim;
	private int nTick = 0;
	private boolean menuOn; // toggle overlay



	// This is the currently selected instrument
	private InstrumentType currentInstrument;

	private final int QUIT = 81, // q key
			LEFT = 37, // left arrow, change instrument
			RIGHT = 39, // right arrow, change instrument
			UP = 38, // increase bars in phrase
			DOWN = 40, // down arrow, decrement bars in phrase
			START = 83, // s key
			UNDO = 32, // space key, pop latest sound
			DEC_BPM = 44, //comma, decrease bpm
			INC_BPM = 46, //period, increase bpm
			TOG_INDIC = 73; //i key, toggle note indicator letters


	// ===============================================
	// ==CONSTRUCTOR
	// ===============================================

	public Game() {

		gmpPanel = new GamePanel(DIM);
		currentInstrument = InstrumentType.byOrdinal(0); // start with Keys
		bpm = 120; // default bpm
		noteIndic = new NoteIndicator(new Point(0,0), InstrumentType.byOrdinal(0));
		timeIndic = new TimeGrid(InstrumentType.values().length, DIM.width, DIM.height, TIME_SIGNATURE,
				bpm);
		menuOn = true; // default is with overlay on
		gmpPanel.addKeyListener(this);
		gmpPanel.addMouseListener(this);
		gmpPanel.addMouseMotionListener(this);

	}

	// ===============================================
	// ==METHODS
	// ===============================================

	public static void init(String args[]) {
		EventQueue.invokeLater(new Runnable() { // uses the Event dispatch thread from Java 5 (refactored)
					public void run() {
						try {
							Game game = new Game(); // construct itself
							game.fireUpAnimThread();

						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}

	private void fireUpAnimThread() { // called initially
		if (thrAnim == null) {
			thrAnim = new Thread(this); // pass the thread a runnable object (this)
			thrAnim.start();
		}
	}

	// implements runnable - must have run method
	public void run() {

		// lower this thread's priority; let the "main" aka 'Event Dispatch'
		// thread do what it needs to do first
		thrAnim.setPriority(Thread.MIN_PRIORITY);

		// and get the current time
		long lStartTime = System.currentTimeMillis();

		// this thread animates the scene
		while (Thread.currentThread() == thrAnim) {
			tick();
			//spawnNewShipFloater();
			gmpPanel.update(gmpPanel.getGraphics(), TIME_SIGNATURE, bpm,
					currentInstrument.interval);
					// update takes the graphics context, plus timesig, bpm, and interval for this instrument
					// we must surround the sleep() in a try/catch block this simply controls delay time between
					// the frames of the animation

			//this might be a good place to check for collisions
			checkCollisions();

			try {
				// The total amount of time is guaranteed to be at least ANI_DELAY long.  If processing (update) 
				// between frames takes longer than ANI_DELAY, then the difference between lStartTime - 
				// System.currentTimeMillis() will be negative, then zero will be the sleep time
				lStartTime += ANI_DELAY;
				Thread.sleep(Math.max(0,
						lStartTime - System.currentTimeMillis()));
			} catch (InterruptedException e) {
				// just skip this frame -- no big deal
				continue;
			}
		} // end while
	} // end run

	private void checkCollisions() {
		/**Commented out as I'm not using any collision checks for this.*/
		/*Point pntFriendCenter, pntFoeCenter;
		int nFriendRadiux, nFoeRadiux;

		for (Movable movFriend : Cc.getInstance().getMovFriends()) {
			for (Movable movFoe : Cc.getInstance().getMovFoes()) {

				pntFriendCenter = movFriend.getCenter();
				pntFoeCenter = movFoe.getCenter();
				nFriendRadiux = movFriend.getRadius();
				nFoeRadiux = movFoe.getRadius();

				//detect collision
				if (pntFriendCenter.distance(pntFoeCenter) < (nFriendRadiux + nFoeRadiux)) {

					Cc.getInstance().getOpsList().enqueue(movFriend, CollisionOp.Operation.REMOVE);

				}//end if 
			}//end inner for
		}//end outer for*/
		


		//we are dequeuing the opsList and performing operations in serial to avoid mutating the movable arraylists
		// while iterating them above
		while(!Cc.getInstance().getOpsList().isEmpty()){
			CollisionOp cop =  Cc.getInstance().getOpsList().dequeue();
			Movable mov = cop.getMovable();
			CollisionOp.Operation operation = cop.getOperation();

			switch (mov.getTeam()){
				case FOE:
					if (operation == CollisionOp.Operation.ADD){
						Cc.getInstance().getMovFoes().add(mov);
					} else {
						Cc.getInstance().getMovFoes().remove(mov);
					}

					break;
				case FRIEND:
					if (operation == CollisionOp.Operation.ADD){
						Cc.getInstance().getMovFriends().add(mov);
					} else {
						Cc.getInstance().getMovFriends().remove(mov);
					}
					break;

				case FLOATER:
					if (operation == CollisionOp.Operation.ADD){
						Cc.getInstance().getMovFloaters().add(mov);
						System.out.println(Cc.getInstance().getMovFloaters());
					} else {
						Cc.getInstance().getMovFloaters().remove(mov);
					}
					break;

				case DEBRIS:
					if (operation == CollisionOp.Operation.ADD){
						Cc.getInstance().getMovDebris().add(mov);
					} else {
						Cc.getInstance().getMovDebris().remove(mov);
					}
					break;


			}

		}
		//a request to the JVM is made every frame to garbage collect, however, the JVM will choose when and how to do this
		System.gc();
		
	}//end meth

	//some methods for timing events in the game,
	//such as the appearance of UFOs, floaters (power-ups), etc. 
	public void tick() {
		if (nTick == Integer.MAX_VALUE)
			nTick = 0;
		else
			nTick++;
	}

	// Called when user presses 's'
	private void startGame() {
		Cc.getInstance().clearAll();
		Cc.getInstance().initGame(TIME_SIGNATURE, bpm);
		Cc.getInstance().getOpsList().enqueue(noteIndic, CollisionOp.Operation.ADD);
		Cc.getInstance().getOpsList().enqueue(timeIndic, CollisionOp.Operation.ADD);
		Cc.getInstance().setPlaying(true);
		Cc.getInstance().setPaused(false);
	}

	// ===============================================
	// KEYLISTENER METHODS
	// ===============================================

	@Override
	public void keyPressed(KeyEvent e) {
		int nKey = e.getKeyCode();

		if (nKey == START && !Cc.getInstance().isPlaying())
			startGame();

		switch (nKey) {
			case QUIT:
				System.exit(0);
				break;

			case UP:
				int intervalu = currentInstrument.interval;
				intervalu++;
				currentInstrument.setInterval(intervalu);
				// we could change the interval for all instances already existing, but this tends
				// to be unpredictable and ugly to implement so I'm not doing it.
				// Instead the interval is changed only for future notes played by this instrument.
				// Cc.getInstance().setRecurrenceInterval(currentInstrument.iType, intervalu);
				break;

			case DOWN:
				int intervald = currentInstrument.interval;
				if (intervald > 1)
					intervald--;
				currentInstrument.setInterval(intervald);
				// we could change the interval for all instances already existing, but this tends
				// to be unpredictable so I'm not doing it.
				// Cc.getInstance().setRecurrenceInterval(currentInstrument.iType, intervald);
				break;

			case LEFT:
				int indexl = currentInstrument.n;
				if (indexl > 0)
					indexl--;
				currentInstrument = InstrumentType.byOrdinal(indexl);
				//gmpPanel.setTitle(currentInstrument.toString());
				noteIndic.setInstType(currentInstrument);
				break;

			case RIGHT:
				int indexr = currentInstrument.n;
				if (indexr < currentInstrument.values().length - 1)
					indexr++;
				currentInstrument = InstrumentType.byOrdinal(indexr);
				//gmpPanel.setTitle(currentInstrument.toString());
				noteIndic.setInstType(currentInstrument);
				break;

			case DEC_BPM:
				if (bpm > 1)
					bpm--;
				Cc.getInstance().setBpm(bpm);
				timeIndic.setTimeBpm(TIME_SIGNATURE, bpm);
				break;

			case INC_BPM:
				bpm++;
				Cc.getInstance().setBpm(bpm);
				timeIndic.setTimeBpm(TIME_SIGNATURE, bpm);
				break;

			case TOG_INDIC:
				noteIndic.hideShow();
				timeIndic.onOff();
				menuOn = !menuOn;
				for (Movable r : Cc.getInstance().getMovFriends()) {
					((Repeater) r).markOn(menuOn);
				}
				break;

			default:
				break;
			}
		}

	@Override
	public void keyReleased(KeyEvent e) {
		int nKey = e.getKeyCode();

		switch (nKey) {
			case UNDO:

				try {
					// if we have any sounds repeating in the queue
					int size = Cc.getInstance().getMovFriends().size();
					if (size > 0) {
						// take the last note added
						Repeater r = (Repeater<Instrument>) Cc.getInstance().getMovFriends().
								get(size - 1);
						// shut down it's thread
						r.shutdown();
						// put it in the queue under remove so the object gets orphaned
						// and the garbage collector can do its thing
						Cc.getInstance().getOpsList().enqueue(r, CollisionOp.Operation.REMOVE);
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				break;

			default:
				break;
			}
	}

	@Override
	// Just need it b/c of KeyListener implementation
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void mouseClicked(MouseEvent e) {

	}

	@Override
	public void mousePressed(MouseEvent e) {
		Point p = e.getPoint();
		// we add a "repeater" to the queue, with the current instrument type, its repeat interval,
		// the time signature, and beats per minute
        Cc.getInstance().getOpsList().enqueue(new Repeater<>(currentInstrument.iType, p, currentInstrument.interval,
						TIME_SIGNATURE, bpm, timeIndic.getX(), menuOn), CollisionOp.Operation.ADD);
	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

	@Override
	public void mouseDragged(MouseEvent e) {

	}

	@Override
	public void mouseMoved(MouseEvent e) {
		noteIndic.setCenter(e.getPoint());
		gmpPanel.setmMouseEvent(e);
	}
}


