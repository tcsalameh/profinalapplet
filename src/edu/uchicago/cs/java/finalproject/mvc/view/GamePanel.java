package edu.uchicago.cs.java.finalproject.mvc.view;

import edu.uchicago.cs.java.finalproject.mvc.controller.Game;
import edu.uchicago.cs.java.finalproject.mvc.model.Cc;
import edu.uchicago.cs.java.finalproject.mvc.model.Movable;
import edu.uchicago.cs.java.finalproject.mvc.model.TimeSignature;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ArrayList;


public class GamePanel extends Panel {
	
	// ==============================================================
	// FIELDS 
	// ============================================================== 
	 
	// The following "off" vars are used for the off-screen double-bufferred image. 
	private Dimension dimOff;
	private Image imgOff;
	private Graphics grpOff;
	
	private GameFrame gmf;
	private Font fnt = new Font("SansSerif", Font.BOLD, 12);
	private Font fntBig = new Font("SansSerif", Font.BOLD + Font.ITALIC, 36);
	private FontMetrics fmt; 
	private int nFontWidth;
	private int nFontHeight;
	private String strDisplay = "";
	
	private MouseEvent mMouseEvent;

	public MouseEvent getmMouseEvent() {
		return mMouseEvent;
	}

	public void setmMouseEvent(MouseEvent mMouseEvent) {
		this.mMouseEvent = mMouseEvent;
	}

	// ==============================================================
	// CONSTRUCTOR 
	// ==============================================================
	
	public GamePanel(Dimension dim){
	    gmf = new GameFrame();
		gmf.getContentPane().add(this);
		//gmf.pack();
		initView();
		
		//gmf.setSize(dim);
		//gmf.setTitle("Keys");
		//gmf.setResizable(false);
		//gmf.setVisible(true);
		this.setFocusable(true);
	}
	
	
	// ==============================================================
	// METHODS 
	// ==============================================================
	
	private void drawScore(Graphics g, TimeSignature ts, int bpm, int interval) {
		g.setColor(Color.white);
		g.setFont(fnt);
		String bars = (interval > 1)? " bars":" bar";
		g.drawString(ts.toString() + " " + bpm + " bpm " + interval + bars,
				nFontWidth, nFontHeight);
	}

	/*public void setTitle(String title) {
		gmf.setTitle(title);
	}*/

	@SuppressWarnings("unchecked")
	public void update(Graphics g, TimeSignature ts, int bpm, int interval) {
		if (grpOff == null || Game.DIM.width != dimOff.width
				|| Game.DIM.height != dimOff.height) {
			dimOff = Game.DIM;
			imgOff = createImage(Game.DIM.width, Game.DIM.height);
			grpOff = imgOff.getGraphics();
		}
		// Fill in background with black.
		grpOff.setColor(Color.black);
		grpOff.fillRect(0, 0, Game.DIM.width, Game.DIM.height);

		drawScore(grpOff, ts, bpm, interval);
		
		if (!Cc.getInstance().isPlaying()) {
			displayTextOnScreen();
		} else if (Cc.getInstance().isPaused()) {
			strDisplay = "Paused";
			grpOff.drawString(strDisplay,
					(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4);
		}
		
		//playing and not paused!
		else {
			
			//draw them in decreasing level of importance
			//friends will be on top layer and debris on the bottom
			iterateMovables(grpOff,
					(ArrayList<Movable>)  Cc.getInstance().getMovFriends(),
					(ArrayList<Movable>)  Cc.getInstance().getMovFoes(),
					(ArrayList<Movable>)  Cc.getInstance().getMovFloaters(),
					(ArrayList<Movable>)  Cc.getInstance().getMovDebris());
		}

		//draw the double-Buffered Image to the graphics context of the panel
		g.drawImage(imgOff, 0, 0, this);
	}

	
	//for each movable array, process it.
	private void iterateMovables(Graphics g, ArrayList<Movable>...movMovz){
		
		for (ArrayList<Movable> movMovs : movMovz) {
			for (Movable mov : movMovs) {
				mov.move();
				mov.draw(g);

			}
		}
		
	}
	
	private void initView() {
		Graphics g = getGraphics();			// get the graphics context for the panel
		g.setFont(fnt);						// take care of some simple font stuff
		fmt = g.getFontMetrics();
		nFontWidth = fmt.getMaxAdvance();
		nFontHeight = fmt.getHeight();
		g.setFont(fntBig);					// set font info
	}
	
	// This method draws some text to the middle of the screen before/after a game
	private void displayTextOnScreen() {
		//TODO: Clean up and add messages about toggle keys

		strDisplay = "Music Generator";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4);

		strDisplay = "See README under profinal directory for more detailed information";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ nFontHeight + 40);

		strDisplay = "'S' to Start";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ nFontHeight + 80);

		strDisplay = "Click to generate a new repeating note";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ nFontHeight + 120);

		strDisplay = "Use space bar to undo last note generated";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ nFontHeight + 160);

		strDisplay = "Use L&R arrow keys to switch between instruments";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ nFontHeight + 200);

		strDisplay = "Use up & down arrow keys to adjust bar length by instrument.";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ nFontHeight + 240);

		strDisplay = "(e.g. a 2 in the top left corner means notes created will repeat every 2 measures.)";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ nFontHeight + 260);

		strDisplay = "Use left and right caret keys (comma and period) to decrease" +
				"or increase the tempo";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ nFontHeight + 300);

		strDisplay = "'i' to toggle informational overlay (note values, metronome bar)";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ nFontHeight + 340);

		strDisplay = "'Q' to Quit";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ nFontHeight + 380);
	}
	
	public GameFrame getFrm() {return this.gmf;}
	public void setFrm(GameFrame frm) {this.gmf = frm;}	
}