package edu.uchicago.cs.java.finalproject.mvc.model;

import javax.sound.sampled.Clip;
import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * Created by tcsalameh on 11/20/15.
 */
public class Instrument extends Sprite {
    private int displayTime; // how long this object should stick around
    private int expandRate; // rate at which animation expands
    private int bucket; // clicked area, numbered 0-15
    public Clip clip;

    public Instrument(Point p, Color c, int dispTime, int expandRate) {
        super();

        setCenter(p);
        setRadius(6);
        setColor(c);

        setExpire(dispTime);

        this.displayTime = dispTime;
        this.expandRate = expandRate;
        this.bucket = getClickRegion();
    }

    /**
     * Game field is subdivided into 16 even squares.
     * This returns the number of the square (0-15) that the click occurred in.
     * @return
     */
    public int getClickRegion() {
        int xMap = getCenter().x - 1;
        int yMap = getCenter().y - 1;
        int xBinSize = getDim().width/4;
        int yBinSize = getDim().height/4;
        int xBin = xMap/xBinSize;
        int yBin = yMap/yBinSize;
        return (xBin + yBin*4);
    }

    public int getBucket() {
        return bucket;
    }

    /**
     * To be overriden by individual instruments
     * @return
     */
    public String[] getSounds() {
        return null;
    }

    /**
     * Default "move" method makes an expanding circle around the center point that
     * moves expandRate units out per frame.
     */
    @Override
    public void move() {
        super.move();

        if (getExpire() == 0) {
            Cc.getInstance().getOpsList().enqueue(this, CollisionOp.Operation.REMOVE);
        }
        else {
            setRadius(getRadius() + expandRate);
            setExpire(getExpire()-1);
        }
    }

    /**
     * Default draw makes a circle that fades as it expands
     * @param g
     */
    @Override
    public void draw(Graphics g) {
        Graphics2D gd = (Graphics2D) g; //cast to Graphics2D so we can manipulate stroke width
        //http://www.java-forums.org/new-java/20553-how-set-line-width-graphics-object.html

        //to get fade effect, we use same color but change alpha based
        //on how close we are to expiry
        float r = (float) (getColor().getRed()/255.0);
        float b = (float) (getColor().getBlue()/255.0);
        float gr = (float) (getColor().getGreen()/255.0);
        float alpha = (float) (((double) getExpire())/displayTime);

        gd.setColor(new Color(r, gr, b, alpha));

        drawGridLines(gd);

        gd.setStroke(new BasicStroke(3F)); //width of line is 3

        int xcoord = getCenter().x - getRadius()/2;
        int ycoord = getCenter().y - getRadius()/2;

        gd.drawOval(xcoord, ycoord, getRadius(), getRadius());
        gd.fillOval(getCenter().x - 2, getCenter().y - 2, 4, 4);

        gd.setStroke(new BasicStroke(1F));

    }

    public void drawGridLines(Graphics2D gd) {
        int height = getDim().height;
        int width = getDim().width;
        gd.drawLine(0, height/4, width, height/4); //1st y bin
        gd.drawLine(0, height/2, width, height/2); //2nd y bin
        gd.drawLine(0, (height/4)*3, width, (height/4)*3); //3rd+4th y bin

        gd.drawLine(width/4, 0, width/4, height); //1st x bin
        gd.drawLine(width/2, 0, width/2, height); //2nd x bin
        gd.drawLine((width/4)*3, 0, (width/4)*3, height); //3rd+4th x bin
    }

    /**
     * To be overridden in individual instruments to reload Clips for repeaters
     */
    public Clip reloadClip() {
        return clip;
    }

    public int getMarkerLoc() {
        return this.getDim().height - 70;
    }

    @Override
    public String toString() {
        return "Instrument{" +
                "displayTime=" + displayTime +
                ", expandRate=" + expandRate +
                ", bucket=" + bucket;
    }
}
