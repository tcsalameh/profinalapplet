package edu.uchicago.cs.java.finalproject.mvc.model;

/**
 * Created by tcsalameh on 11/20/15.
 */
public class TimeSignature {
    private int numerator;
    private int denom;

    public TimeSignature(int num, int denom) {
        this.numerator = num;
        this.denom = denom;
    }

    public int getNumerator() {
        return numerator;
    }

    public int getDenom() {
        return denom;
    }

    /**
     * Calculates total milliseconds per measure, based on time signature and bpm.
     * @param bpm
     */
    public int getMillisecsPerBar(int bpm) {
        double whole = (60.0/bpm)*4; //whole note duration
        double wholems = whole*1000.0; //milliseconds
        int duration = (int) (numerator*wholems/denom); //total time is whole note*signature
        return duration;
    }

    @Override
    public String toString() {
        return numerator + "/" + denom;
    }
}
