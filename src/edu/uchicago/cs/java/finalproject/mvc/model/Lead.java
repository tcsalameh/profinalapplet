package edu.uchicago.cs.java.finalproject.mvc.model;

import edu.uchicago.cs.java.finalproject.sounds.Sound;

import javax.sound.sampled.Clip;
import java.awt.*;
import java.nio.file.Paths;

/**
 * Created by tcsalameh on 11/21/15.
 */
public class Lead extends Instrument {
    private static final int EXPIRE = 40; //expiry time
    private static final int RATE = 5; //expansion rate
    private static final Color COLOR = new Color(242,239,55);
    private static final String[] SOUNDS = {Paths.get("lead","Widen C4.wav").toString(), //C
            Paths.get("lead","Widen C#4.wav").toString(), //C#
            Paths.get("lead","Widen D4.wav").toString(), //D
            Paths.get("lead","Widen D#4.wav").toString(), //D#
            Paths.get("lead","Widen E4.wav").toString(), //E
            Paths.get("lead","Widen F4.wav").toString(), //F
            Paths.get("lead","Widen F#4.wav").toString(), //F#
            Paths.get("lead","Widen G4.wav").toString(), //G
            Paths.get("lead","Widen G#4.wav").toString(), //G#
            Paths.get("lead","Widen A4.wav").toString(), //A
            Paths.get("lead","Widen A#4.wav").toString(), //A#
            Paths.get("lead","Widen B4.wav").toString(), //B
            Paths.get("lead","Widen C5.wav").toString(), //C
            Paths.get("lead","Widen C#5.wav").toString(), //C#
            Paths.get("lead","Widen D5.wav").toString(), //D
            Paths.get("lead","Widen D#5.wav").toString()}; //D#2
    
    public Lead(Point p, Clip clp) {
        super(p, COLOR, EXPIRE, RATE);
        setTeam(Team.FRIEND);
        if (clp == null)
            clp = Sound.loadClip(SOUNDS[super.getBucket()]);
        super.clip = clp;
        Sound.playSound(clp);
    }

    @Override
    public String[] getSounds() {
        return SOUNDS;
    }

    public Clip reloadClip() {
        return Sound.loadClip(SOUNDS[super.getBucket()]);
    }

    @Override
    public int getMarkerLoc() {
        return getDim().height - 30;
    }
}

