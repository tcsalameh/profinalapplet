package edu.uchicago.cs.java.finalproject.mvc.model;

import java.awt.*;
import java.sql.Time;

/**
 * Created by tcsalameh on 11/26/15.
 * Draws static lanes in which to place upcoming notes, can be toggled on and off.
 */
public class TimeGrid implements Movable {
    private static final Team team = Team.FLOATER;

    private int bpm;
    private int millisecsPerBar;
    private long startTime;

    private int yStart;
    private int yEnd;
    private int xEnd;
    private int xCurr;

    private int nLines;

    private boolean on;

    public TimeGrid(int nInstruments, int xEnd, int yEnd, TimeSignature t, int bpm) {
        this.on = true;
        this.nLines = nInstruments;
        this.xEnd = xEnd;
        this.yEnd = yEnd;
        this.yStart = yEnd - nInstruments*10 - 20;
        this.xCurr = 0;
        this.startTime = System.currentTimeMillis();
        setTimeBpm(t, bpm);
    }

    public void setTimeBpm(TimeSignature t, int bpm) {
        this.bpm = bpm;
        this.millisecsPerBar = t.getMillisecsPerBar(this.bpm);
    }

    public void onOff() {
        this.on = !this.on;
    }

    public int getX() {
        return xCurr;
    }

    @Override
    public void move() {
        // movement is independent of frame rate
        long elapsed = (System.currentTimeMillis() - startTime);
        long timePos = elapsed % millisecsPerBar;
        this.xCurr = (int) (((double) xEnd/millisecsPerBar)*timePos);
    }

    @Override
    public void draw(Graphics g) {
        if (on) {
            g.setColor(Color.GRAY);
            for (int i = 0; i < nLines; i++) {
                g.drawLine(0, yStart + i * 10, xEnd, yStart + i * 10);
            }
            g.drawLine(xCurr, yStart, xCurr, yEnd);
        }
        else {
            g.setColor(Color.BLACK);
        }

    }

    @Override
    public Point getCenter() {
        return null;
    }

    @Override
    public int getRadius() {
        return 0;
    }

    @Override
    public Team getTeam() {
        return this.team;
    }
}
